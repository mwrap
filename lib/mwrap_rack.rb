# Copyright (C) all contributors <mwrap-public@80x24.org>
# License: GPL-2.0+ <https://www.gnu.org/licenses/gpl-2.0.txt>
# frozen_string_literal: true
require 'mwrap'
require 'rack'
require 'cgi'

# MwrapRack is an obsolete standalone Rack application which can be
# mounted to run within your application process.
#
# The embedded mwrap-httpd for Unix sockets and mwrap-rproxy for TCP
# from the Perl version <https://80x24.org/mwrap-perl.git/> replaces
# this in a non-obtrusive way for code which can't handle Ruby-level
# threads.
#
# The remaining documentation remains for historical purposes:
#
# Using the Rack::Builder API in config.ru, you can map it to
# the "/MWRAP/" endpoint.  As with the rest of the Mwrap API,
# your Rack server needs to be spawned with the mwrap(1)
# wrapper to enable the LD_PRELOAD.
#
#     require 'mwrap_rack'
#     map('/MWRAP') { run(MwrapRack.new) }
#     map('/') { run(your_normal_app) }
#
# This module is only available in mwrap 2.0.0+
class MwrapRack
  module HtmlResponse # :nodoc:
    def response
      [ 200, {
          'expires' => 'Fri, 01 Jan 1980 00:00:00 GMT',
          'pragma' => 'no-cache',
          'cache-control' => 'no-cache, max-age=0, must-revalidate',
          'content-type' => 'text/html; charset=UTF-8',
        }, self ]
    end
  end

  class Each < Struct.new(:script_name, :min, :sort) # :nodoc:
    include HtmlResponse
    HEADER = '<tr><th>' + %w(total allocations frees mean_life max_life
                location).join('</th><th>') + '</th></tr>'
    FIELDS = %w(total allocations frees mean_life max_life location)
    def each
      Mwrap.quiet do
        t = -"Mwrap.each(#{min})"
        sn = script_name
        all = []
        f = FIELDS.dup
        sc = FIELDS.index(sort || 'total') || 0
        f[sc] = -"<b>#{f[sc]}</b>"
        f.map! do |hdr|
          if hdr.start_with?('<b>')
            hdr
          else
            -%Q(<a\nhref="#{sn}/each/#{min}?sort=#{hdr}">#{hdr}</a>)
          end
        end
        Mwrap.each(min) do |loc, total, allocations, frees, age_sum, max_life|
          mean_life = frees == 0 ? Float::INFINITY : age_sum/frees.to_f
          all << [total,allocations,frees,mean_life,max_life,loc]
        end
        all.sort_by! { |cols| -cols[sc] }

        yield(-"<html><head><title>#{t}</title></head>" \
               "<body><h1>#{t}</h1>\n" \
               "<h2>Current generation: #{GC.count}</h2>\n<table>\n" \
               "<tr><th>#{f.join('</th><th>')}</th></tr>\n")
        all.each do |cols|
          loc = cols.pop
          cols[3] = sprintf('%0.3f', cols[3]) # mean_life
          href = -(+"#{sn}/at/#{CGI.escape(loc)}").encode!(xml: :attr)
          yield(%Q(<tr><td>#{cols.join('</td><td>')}<td><a\nhref=#{
                  href}>#{-loc.encode(xml: :text)}</a></td></tr>\n))
          cols.clear
        end.clear
        yield "</table></body></html>\n"
      end
    end
  end

  class EachAt < Struct.new(:loc) # :nodoc:
    include HtmlResponse
    HEADER = '<tr><th>size</th><th>generation</th></tr>'

    def each
      t = loc.name.encode(xml: :text)
      yield(-"<html><head><title>#{t}</title></head>" \
             "<body><h1>live allocations at #{t}</h1>" \
             "<h2>Current generation: #{GC.count}</h2>\n<table>#{HEADER}")
      loc.each do |size, generation|
        yield("<tr><td>#{size}</td><td>#{generation}</td></tr>\n")
      end
      yield "</table></body></html>\n"
    end
  end

  def r404 # :nodoc:
    [404,{'content-type'=>'text/plain'},["Not found\n"]]
  end

  # The standard Rack application endpoint for MwrapRack
  def call(env)
    case env['PATH_INFO']
    when %r{\A/each/(\d+)\z}
      min = $1.to_i
      m = env['QUERY_STRING'].match(/\bsort=(\w+)/)
      Each.new(env['SCRIPT_NAME'], min, m ? m[1] : nil).response
    when %r{\A/at/(.*)\z}
      loc = -CGI.unescape($1)
      loc = Mwrap[loc] or return r404
      EachAt.new(loc).response
    when '/'
      n = 2000
      u = 'https://80x24.org/mwrap/README.html'
      b = -('<html><head><title>Mwrap demo</title></head>' \
          "<body><p><a href=\"each/#{n}\">allocations &gt;#{n} bytes</a>" \
          "<p><a href=\"#{u}\">#{u}</a>" \
          "</body></html>\n")
      [ 200, {'content-type'=>'text/html','content-length'=>-b.size.to_s},[b]]
    else
      r404
    end
  end
end
