# Copyright (C) mwrap hackers <mwrap-public@80x24.org>
# License: GPL-2.0+ <https://www.gnu.org/licenses/gpl-2.0.txt>
require 'rake/testtask'
begin
  require 'rake/extensiontask'
  Rake::ExtensionTask.new('mwrap')
rescue LoadError
  warn 'rake-compiler not available, cross compiling disabled'
end

Rake::TestTask.new('test-ruby')
task 'test-ruby' => :compile
task :default => :compile

task 'test-httpd': 'lib/mwrap.so' do
  require 'rbconfig'
  ENV['RUBY'] = RbConfig.ruby
  sh "#{ENV['PROVE'] || 'prove'} -v"
end

task test: %w(test-ruby test-httpd)

c_files = File.readlines('MANIFEST').grep(%r{ext/.*\.[ch]$}).map!(&:chomp!)
task 'compile:mwrap' => c_files

olddoc = ENV['OLDDOC'] || 'olddoc'
rdoc = ENV['RDOC'] || 'rdoc'
task :rsync_docs do
  require 'fileutils'
  top = %w(README COPYING LATEST NEWS NEWS.atom.xml)
  system("git", "set-file-times")
  dest = ENV["RSYNC_DEST"] || "80x24.org:/srv/80x24/mwrap/"
  FileUtils.rm_rf('doc')
  sh "#{olddoc} prepare"
  sh "#{rdoc} -f dark216" # dark216 requires olddoc 1.7+
  File.unlink('doc/created.rid') rescue nil
  File.unlink('doc/index.html') rescue nil
  FileUtils.cp(top, 'doc')
  sh "#{olddoc} merge"

  Dir['doc/**/*'].each do |txt|
    st = File.stat(txt)
    if st.file?
      gz = "#{txt}.gz"
      tmp = "#{gz}.#$$"
      sh("gzip --rsyncable -9 <#{txt} >#{tmp}")
      File.utime(st.atime, st.mtime, tmp) # make nginx gzip_static happy
      File.rename(tmp, gz)
    end
  end
  sh("rsync --chmod=Fugo=r #{ENV['RSYNC_OPT']} -av doc/ #{dest}/")
end
