#!perl -w
# Copyright (C) mwrap hackers <mwrap-perl@80x24.org>
# License: GPL-3.0+ <https://www.gnu.org/licenses/gpl-3.0.txt>
use v5.12;
use IO::Socket::UNIX;
use Fcntl qw(F_GETFD F_SETFD FD_CLOEXEC);
use POSIX qw(dup2 _exit mkfifo);
BEGIN { require './t/test_common.perl' };
my $env = { MWRAP => "socket_dir:$mwrap_tmp" };
my $f1 = "$mwrap_tmp/f1";
my $f2 = "$mwrap_tmp/f2";
mkfifo($f1, 0600) // plan(skip_all => "mkfifo: $!");
mkfifo($f2, 0600) // plan(skip_all => "mkfifo: $!");
my $src = $mwrap_src ? # $mwrap_src is Perl-only, Ruby otherwise
	"open my \$f1, '>', '$f1'; close \$f1; open my \$f2, '<', '$f2'" :
	"File.open('$f1', 'w').close; File.open('$f2', 'r').close";
my $pid = mwrap_run('httpd test', $env, '-e', $src);
my $spid;
my $mw_exit;
my $cleanup = sub {
	if (defined $spid) {
		if (kill('TERM', $spid)) {
			waitpid($spid, 0);
			$? == 0 or warn "rproxy died with \$?=$?";
		} else {
			warn "kill $spid: $!";
		}
		undef $spid;
	}
	use autodie;
	if (defined $pid) {
		my $exit = $?;
		open my $fh, '>', $f2;
		close $fh;
		waitpid($pid, 0);
		$mw_exit = $?;
		undef $pid;
		diag "err: ".slurp($mwrap_err);
		$? = $exit;
	}
};
END { $cleanup->() }

my $sock = "$mwrap_tmp/$pid.sock";
my %o = (Peer => $sock , Type => SOCK_STREAM);
local $SIG{PIPE} = 'IGNORE';

open my $fh, '<', $f1;
is(my $nil = <$fh>, undef, 'FIFO open');
close $fh;
ok(-S $sock, 'socket created');
my $c = IO::Socket::UNIX->new(%o);
ok($c, 'socket connected');
is(send($c, 'GET', MSG_NOSIGNAL), 3, 'trickled 3 bytes') or diag "send: $!";

my $cout = "$mwrap_tmp/cout";
my @curl = (qw(curl -sf --unix-socket), $sock, '-o', $cout);
push @curl, '-vS' if $ENV{V};
my $rc = system(@curl, "http://0/$pid/each/2000");
my $curl_unix;
SKIP: {
	skip 'curl lacks --unix-socket support', 1 if $rc == 512;
	is($rc, 0, 'curl /each');
	unlink($cout);
	$curl_unix = 1;

	$rc = system(@curl, "http://0/$pid/each/2000");
	is($rc, 0, 'curl /each');
	unlink($cout);

	$rc = system(@curl, "http://0/$pid/");
	is($rc, 0, 'curl / (PID root)');
	like(slurp($cout), qr/<html>/, 'root shown');

	$rc = system(@curl, '-XPOST', "http://0/$pid/trim");
	is($rc, 0, 'curl / (PID root)');
	like(slurp($cout), qr/trimming/, 'trim started');
	unlink($cout);
};

{
	my $req = " /$pid/each/20000 HTTP/1.0\r\n\r\n";
	is(send($c, $req, MSG_NOSIGNAL), length($req),
		'wrote rest of response') or diag "send: $!";
	my $x = do { local $/; <$c> } or diag "readline: $!";
	like($x, qr!</html>\n?\z!s, 'got complete HTML response');
}

SKIP: {
	my (@rproxy, @missing);
	if (-e 'script/mwrap-rproxy') { # Perl version
		@rproxy = ($^X, '-w', './blib/script/mwrap-rproxy');
	} else {
		my $exe = `which mwrap-rproxy`;
		if ($? == 0 && defined($exe)) {
			chomp($rproxy[0] = $exe);
		} else {
			push @missing, 'mwrap-rproxy';
		}
	}
	for my $m (qw(Plack::Util HTTP::Tiny)) {
		eval "require $m" or push(@missing, $m);
	}
	skip join(', ', @missing).' missing', 1 if @missing;
	my $srv = IO::Socket::INET->new(LocalAddr => '127.0.0.1',
				ReuseAddr => 1, Proto => 'tcp',
				Type => SOCK_STREAM,
				Listen => 1024);
	$spid = fork;
	if ($spid == 0) {
		local $ENV{LISTEN_PID} = $$;
		local $ENV{LISTEN_FDS} = 1;
		my $fl = fcntl($srv, F_GETFD, 0);
		fcntl($srv, F_SETFD, $fl &= ~FD_CLOEXEC);
		if (fileno($srv) != 3) {
			dup2(fileno($srv), 3) or die "dup2: $!";
		}
		local $ENV{PLACK_ENV} = 'deployment' if !$ENV{V};
		no warnings 'exec';
		exec @rproxy, "--socket-dir=$mwrap_tmp";
		_exit(1);
	}
	my $http = HTTP::Tiny->new;
	my ($h, $p) = ($srv->sockhost, $srv->sockport);
	undef $srv;
	my $res = $http->get("http://$h:$p/");
	ok($res->{success}, 'listing success');
	like($res->{content}, qr!/$pid/each/\d+!, 'got listing for each');
	$res = $http->get("http://$h:$p/$pid/each/1");
	ok($res->{success}, 'each/1 success');
	my $t = '/at/$LOCATION link in /each/$NUM';
	if ($res->{content} =~ m!href="\.\./at/([^"]+)"!) {
		my $loc = $1;
		ok($t);
		$res = $http->get("http://$h:$p/$pid/at/$1");
		ok($res->{success}, '/at/$LOCATION endpoint');
		like($res->{content}, qr!\blive allocations at\b!,
			'live allocations shown');
	} else {
		fail($t);
	}
	if ($ENV{INTERACTIVE}) {
		diag "http://$h:$p/$pid/each/1 up for interactive testing";
		diag "- press Enter when done -";
		my $ok = <STDIN>;
	}
}

SKIP: {
	skip 'no reset w/o curl --unix-socket', 1 if !$curl_unix;
	my ($sqlite_v) = (`sqlite3 --version` =~ /([\d+\.]+)/);
	if ($?) {
		diag 'sqlite3 missing or broken';
		$sqlite_v = 0;
	} else {
		my @v = split(/\./, $sqlite_v);
		$sqlite_v = ($v[0] << 16) | ($v[1] << 8) | $v[2];
		diag 'sqlite_v='.sprintf('0x%x', $sqlite_v);
	}
	$rc = system(@curl, "http://0/$pid/each/100.csv");
	is($rc, 0, '.csv retrieved') or skip 'CSV failed', 1;
	my $db = "$mwrap_tmp/t.sqlite3";

	if ($sqlite_v >= 0x32000) {
		$rc = system(qw(sqlite3), $db,".import --csv $cout mwrap_each");
		is($rc, 0, 'sqlite3 import');
		my $n = `sqlite3 $db 'SELECT COUNT(*) FROM mwrap_each'`;
		is($?, 0, 'sqlite3 count');
		my $exp = split(/\n/, slurp($cout));
		is($n + 1, $exp, 'imported all rows into sqlite');
	} else {
		diag "sqlite 3.32.0+ needed for `.import --csv'";
	}

	$rc = system(@curl, qw(-d x=y), "http://0/$pid/reset");
	is($rc, 0, 'curl /reset');
	$rc = system(@curl, qw(-HX-Mwrap-BT:10 -XPOST),
			"http://0/$pid/ctl");
	is($rc, 0, 'curl /ctl (X-Mwrap-BT)');
	like(slurp($cout), qr/\bMWRAP=bt:10\b/, 'changed bt depth');

	$rc = system(@curl, qw(-HX-Mwrap-BT:10 -d blah http://0/ctl));
	is($rc >> 8, 22, '404 w/o PID prefix');
};


diag slurp($cout) if $ENV{V};
$cleanup->();
ok(!-e $sock, 'socket unlinked after cleanup');
is($mw_exit, 0, 'perl exited with $?==0');
done_testing;
