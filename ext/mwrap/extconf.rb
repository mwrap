# frozen_string_literal: true
# Copyright (C) mwrap hackers <mwrap-public@80x24.org>
# License: GPL-2.0+ <https://www.gnu.org/licenses/gpl-2.0.txt>
require 'mkmf'

have_func 'mempcpy'
have_library 'urcu-cds' or abort 'userspace RCU not installed'
have_header 'urcu/rculfhash.h' or abort 'rculfhash.h not found'
have_library 'urcu-bp' or abort 'liburcu-bp not found'
have_library 'execinfo' # FreeBSD
$defs << '-DHAVE_XXHASH' if have_header('xxhash.h')

have_struct_member('struct sockaddr_un', 'sun_len', %w(sys/socket sys/un.h))

if try_link(<<EOC)
int main(void) { return __builtin_add_overflow_p(0,0,(int)1); }
EOC
  $defs << '-DHAVE_BUILTIN_ADD_OVERFLOW_P'
end

if try_link(<<EOC)
int main(int a) { return __builtin_add_overflow(0,0,&a); }
EOC
  $defs << '-DHAVE_BUILTIN_ADD_OVERFLOW_P'
else
  abort 'missing __builtin_add_overflow'
end

create_makefile 'mwrap'
