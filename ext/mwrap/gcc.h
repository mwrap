/* CC0 (Public domain) - http://creativecommons.org/publicdomain/zero/1.0/ */
#ifndef GCC_H
#define GCC_H
#define ATTR_COLD	__attribute__((cold))

#if __STDC_VERSION__ >= 201112
#	define MWRAP_TSD _Thread_local
#elif defined(__GNUC__)
#	define MWRAP_TSD __thread
#else
#	error _Thread_local nor __thread supported
#endif
#endif /* GCC_H */
