#ifndef CHECK_H
#define CHECK_H
#include "gcc.h"
#include <stdlib.h>
#include <assert.h>
/*
 * standard assert may malloc, but NDEBUG behavior is standardized,
 * however Perl headers add some weirdness if we undef NDEBUG, so
 * keep NDEBUG defined and use MWRAP_NO_DEBUG
 */
#if defined(NDEBUG) && defined(MWRAP_NO_DEBUG)
#  define mwrap_assert(expr)
#  define CHECK(type, expect, expr) (void)(expr)
#else
#  define mwrap_assert(x) do { if (caa_unlikely(!(x))) abort(); } while (0)
#  define CHECK(type, expect, expr) do { \
	type checkvar = (expr); \
	mwrap_assert(checkvar==(expect)&& "BUG" && __FILE__ && __LINE__); \
	(void)checkvar; \
	} while (0)
#endif

#endif /* CHECK_H */
